<div dir="ltr" style="text-align: left;" trbidi="on">
<h5 style="background-color: white; box-sizing: border-box; color: #222222; font-family: Poppins; font-size: 14px; font-weight: 400; line-height: 18px; margin-bottom: 10px; margin-top: 0px;">
Quick overview:</h5>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
Booking your own holiday is a lot cheaper than booking through a travel agent. However, doing all the research needed to plan your own trip can be extremely time-consuming. With the support of the&nbsp;<a href="http://scriptstore.in/" style="background-color: transparent; box-sizing: border-box; color: #222222; text-decoration-line: none; transition: all 300ms ease;">holiday script</a>, you will save time as well as money, and you will visit beautiful places.</div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<br /></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<br /></div>
<div class="std" itemprop="description" style="background-color: white; box-sizing: border-box; color: #666666; font-family: Arial; font-size: 13px; line-height: 2em;">
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; color: #999999; float: left; font-family: Roboto, sans-serif; font-size: 16px; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
UNIQUE FEATURES:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<ul style="box-sizing: border-box; margin-bottom: 10px; margin-top: 0px;">
<li style="box-sizing: border-box;">Super Admin</li>
<li style="box-sizing: border-box;">Affiliations booking modules</li>
<li style="box-sizing: border-box;">Wallet user modules</li>
<li style="box-sizing: border-box;">Seat seller</li>
<li style="box-sizing: border-box;">White label</li>
<li style="box-sizing: border-box;">Multiple API integrations</li>
<li style="box-sizing: border-box;">Own bus inventory management</li>
<li style="box-sizing: border-box;">Unique Mark up, Commission and service charge for agent wise</li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-sm-6 wpb_column vc_column_container" style="box-sizing: border-box; color: #999999; float: left; font-family: Roboto, sans-serif; font-size: 16px; min-height: 1px; padding-left: 15px; padding-right: 15px; position: relative; width: 559px;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<h4 class="vc_custom_heading" style="box-sizing: border-box; color: #26547c; font-size: 18px; font-weight: 400; line-height: 20px; margin: 0px 0px 15px;">
GENERAL FEATURES:</h4>
<div class="wpb_text_column wpb_content_element " style="box-sizing: border-box;">
<div class="wpb_wrapper" style="box-sizing: border-box;">
<ul style="box-sizing: border-box; margin-bottom: 10px; margin-top: 0px;">
<li style="box-sizing: border-box;">Highly Customizable and Flexible</li>
<li style="box-sizing: border-box;">Wallet payment</li>
<li style="box-sizing: border-box;">Guest user access</li>
<li style="box-sizing: border-box;">User-friendly Interface</li>
<li style="box-sizing: border-box;">Eye-catching ui design and color combination</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
